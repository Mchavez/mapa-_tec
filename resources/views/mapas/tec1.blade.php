<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
          <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="MAPA ITMOTELIA">
        <meta name="author" content="Equipo3">
        <title>Mapa Campus 1</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- Custom styles for this template -->
        <link href="album.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.pannellum.org/2.4/pannellum.css"/>

        <style>
        #panorama {
            width: 1200px;
            height: 500px;
        }
        </style>
    </head>
    <body>
    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white">Misión</h4>
              <p class="text-muted">Contribuir al desarrollo de México formando recursos humanos altamente competitivos a nivel internacional que satisfagan la demanda de la sociedad, a través de sus programas de docencia, investigación y vinculación, integrados con la cultura, el deporte y el cuidado del medio ambiente, en un contexto de calidad, respeto, armonía, que reflejan los valores de la comunidad institucional.</p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Contacto</h4>
              <ul class="list-unstyled">
                <li><a href="#" class="text-white">Pagina</a></li>
                  <ul>
                    <li><a href="http://www.itmorelia.edu.mx/" target=”_blank” class="text-muted">Itmorelia.edu.mx</a></li>
                  </ul>
                <li><a  class="text-white">Telefono</a></li>
                  <ul>
                    <li><a class="text-muted">01 443 312 1570</a> </li>
                  </ul>
                <li><a class="text-white">Ubicacion</a></li>
                  <ul>
                    <li><a href="https://goo.gl/maps/izqWEwUg9f92"  target=”_blank” class="text-muted">ITMorelia</a> </li>
                  </ul>
                
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
            <a href="#" class="navbar-brand d-flex align-items-center">
            <img src="https://upload.wikimedia.org/wikipedia/commons/9/91/Itmorelia.png" alt="Smiley face" height="50" width="55" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2">
            <strong>  Inicio</strong>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
        </div>
      </div>
    </header>

    <main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Instituto Tecnológico de Morelia</h1>
          <p class="lead text-muted">Mapa Campus I</p>
          <!-- <p>
            <a href="#" class="btn btn-primary my-2">Main call to action</a>
            <a href="#" class="btn btn-secondary my-2">Secondary action</a>
          </p> -->
        </div>
      </section>

                
      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
            <div id="panorama"></div>
            <div class="col-md-4">
              
              <div class="card mb-4 box-shadow">
              </div>
            </div>
            </div>
        </div>
    </div>
</main>

    </body>
     <script type="text/javascript" src="https://cdn.pannellum.org/2.4/pannellum.js">
        </script>
     <script src="js/mapa.js"> </script>
       <!-- Bootstrap core Scripts-->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</html>
