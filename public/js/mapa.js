  
   pannellum.viewer('panorama', {   
            "default": {
                "firstScene": "1",
                "sceneFadeDuration": 2300
            },
            //<img src"img/A1.jpg">

            "scenes": {
                "1":{
                    "title": "Campus 1",
                    "hfov": 110,
                    "yaw": 5,
                    "type": "equirectangular",
                    "panorama":"img/1.jpg",
                    "hotSpots": [
                    {
                        "pitch": -1,
                        "yaw": 2.3,
                        "type": "scene",
                        // "text": "Edificio A",
                        "sceneId": "1.1"
                    }]         
                    },
                "1.1": {
                    "title": "A",
                    "hfov": 110,//zoom
                    "yaw": 0,//posicion en la que se ve en grados
                    "type": "equirectangular",
                    "panorama": "img/1.1.jpg",
                    "hotSpots": [
                    {
                        "pitch": 4.6,
                        "yaw": -12,
                        "type": "scene",
                        "text": "",
                        "sceneId": "1.2",
                        "targetYaw": 180,
                        "targetPitch": 2
                    },
                    {
                        "pitch": 2.6,
                        "yaw": 185,
                        "type": "scene",
                        // "text": "Entrada",
                        "sceneId": "1",
                        "targetYaw": 180,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },
                "1.2": {
                       "title": "Campus 1",
                        "hfov": 110,//zoom
                        "yaw": 0,//posicion en la que se ve en grados
                        "type": "equirectangular",
                        "panorama": "img/1.2.jpg",
                        "hotSpots": [
                    {
                        "pitch": 4.6,
                        "yaw": 145,
                        "type": "scene",
                        "text": "",
                        "sceneId": "2",
                        "targetYaw": -23,
                        "targetPitch": 2
                    },
                    {
                        "pitch": 2.6,
                        "yaw": 0,
                        "type": "scene",
                        "text": "",
                        "sceneId": "1.1",
                        "targetYaw": 180,//posicion de regreso en garados
                        "targetPitch": 2  
                    },
                    {
                        "pitch": 2.6,
                        "yaw": -157,
                        "type": "scene",
                        "text": "",
                        "sceneId": "14",
                        "targetYaw":0,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },

                "2":{
                    "title": "Campus 1",
                    "hfov": 110,
                    "yaw": 5,
                    "type": "equirectangular",
                    "panorama":"img/2.jpg",
                    "hotSpots": [
                    {
                        "pitch": 2,
                        "yaw": -8,
                        "type": "scene",
                        "text": "",
                        "sceneId": "3",
                        "targetYaw": -23,
                        "targetPitch": 2
                    },
                     {
                        "pitch": -0.6,
                        "yaw": 25,
                        "type": "scene",
                        "text": "",
                        "sceneId": "3.1",
                        "targetYaw": 180,
                        "targetPitch": 2
                    },
                    {
                        "pitch":0.6,
                        "yaw": -135,
                        "type": "scene",
                        "text": "",
                        "sceneId": "1.1",
                        "targetYaw": 180,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },


                "3": {
                    "title": "Campus 1",
                    "hfov": 110,//zoom
                    "yaw": 0,//posicion en la que se ve en grados
                    "type": "equirectangular",
                    "panorama": "img/3.jpg",
                    "hotSpots": [
                    {
                        "pitch": -0.6,
                        "yaw": 0,
                        "type": "scene",
                        "text": "",
                        "sceneId": "4",
                        "targetYaw": -23,
                        "targetPitch": 2
                    },
                    {
                        "pitch": -0.6,
                        "yaw": 175,
                        "type": "scene",
                        "text": "",
                        "sceneId": "2",
                        "targetYaw": 180,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },
                "3.1": {
                    "title": "Campus 1",
                    "hfov": 110,//zoom
                    "yaw": 180,//posicion en la que se ve en grados
                    "type": "equirectangular",
                    "panorama": "img/3.1.jpg",
                    "hotSpots": [
                    {
                        "pitch": 3,
                        "yaw": 182,
                        "type": "scene",
                        "text": "",
                        "sceneId": "3.2",
                        "targetYaw": 85,
                        "targetPitch": 2
                    },
                    {
                        "pitch": -4.8,
                        "yaw":0,
                        "type": "scene",
                        "text": "",
                        "sceneId": "2",
                        "targetYaw": 180,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },
                "3.2": {
                    "title": "Campus 1",
                    "hfov": 110,//zoom
                    "yaw": 0,//posicion en la que se ve en grados
                    "type": "equirectangular",
                    "panorama": "img/3.2.jpg",
                    "hotSpots": [
                    {///F
                        "pitch": -0.6,
                        "yaw":93 ,
                        "type": "scene",
                        "text": "",
                        "sceneId": "6",
                        "targetYaw":0,
                        "targetPitch": 2
                    },
                    {
                        "pitch": -3.3,
                        "yaw": 273,
                        "type": "scene",
                        "text": "",
                        "sceneId": "3.1",
                        "targetYaw": 0,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },
                "4":{
                    "title": "Campus 1",
                    "hfov": 110,
                    "yaw": 0,
                    "type": "equirectangular",
                    "panorama": "img/4.jpg",
                    "hotSpots": [
                    {
                        "pitch": -2.1,
                        "yaw": 0,
                        "type": "scene",
                        "text": "",
                        "sceneId": "A"
                    },
                    {
                        "pitch": -0.6,
                        "yaw": -245,
                        "type": "scene",
                        "text": "",
                        "sceneId": "3",
                        "targetYaw": 180,//posicion de regreso en garados
                        "targetPitch": 2  
                    }] 
                    },
                "A":{
                    "title": "Edificio A",
                    "hfov": 110,
                    "yaw": 0,
                    "type": "equirectangular",
                    "panorama": "img/A.jpg",
                    "hotSpots": [
                    {
                        "pitch": -2.1,
                        "yaw": 7,
                        "type": "scene",
                        "text": "",
                        "sceneId": "5"
                    },
                    {
                        "pitch": -0.6,
                        "yaw": 188,
                        "type": "scene",
                        "text": "",
                        "sceneId": "4",
                        "targetYaw": 180,//posicion de regreso en garados
                        "targetPitch": 2  
                    }] 
                    },
                "5":{  
                    "title": "Campus 1",
                    "hfov": 100,
                    "yaw": 5,
                    "type": "equirectangular",
                    "panorama": "img/5.jpg",
                    "hotSpots": [
                    {//aun no foto
                        "pitch": 1,
                        "yaw": 90,
                        "type": "scene",
                        "text": "",
                        "sceneId": "6.2",
                        "targetYaw": 180,
                        "targetPitch": 2 
                    },
                    {
                        "pitch": -0.6,
                        "yaw": 180,
                        "type": "scene",
                        "text": "",
                        "sceneId": "A",
                        "targetYaw": 180,
                        "targetPitch": 2  
                    }] 
                    },
                "6":{  
                    "title": "Edificio F",
                    "hfov": 100,
                    "yaw": 5,
                    "type": "equirectangular",
                    "panorama": "img/6.jpg",
                    "hotSpots": [
                    {
                        "pitch": 1,
                        "yaw": 0,
                        "type": "scene",
                        "text": "",
                        "sceneId": "6.1",
                        "targetYaw":87,
                        "targetPitch": 2 

                    },
                    {
                        "pitch": -0.6,
                        "yaw": 180,
                        "type": "scene",
                        "text": " ",
                        "sceneId": "3.2",
                        "targetYaw":-45,
                        "targetPitch": 2  
                    },
                    {
                        "pitch": -0.6,
                        "yaw": -270,
                        "type": "scene",
                        "text": "",
                        "sceneId": "15",
                        "targetYaw":250,
                        "targetPitch": 2  
                    }] 
                    },
                "6.1":{  
                    "title": "Edificio F",
                    "hfov": 100,
                    "yaw": 5,
                    "type": "equirectangular",
                    "panorama": "img/6.1.jpg",
                    "hotSpots": [
                    {
                        "pitch": 1,
                        "yaw": 4,
                        "type": "scene",
                        "text": "",
                        "sceneId": "6.2"
                    },

                    {
                        "pitch": -2.6,
                        "yaw": -100,
                        "type": "scene",
                        "text": "",
                        "sceneId": "6",
                        "targetYaw":180,
                        "targetPitch": 2  
                    },
                    {
                        "pitch": -0.6,
                        "yaw": 94,
                        "type": "scene",
                        "text": "",
                        "sceneId": "7",
                        "targetYaw":5,
                        "targetPitch": 2  
                    }] 
                    },
                "6.2":{  
                    "title": "Campus 1",
                    "hfov": 100,
                    "yaw": 5,
                    "type": "equirectangular",
                    "panorama": "img/6.2.jpg",
                    "hotSpots": [
                    {
                        "pitch": -1,
                        "yaw": 0,
                        "type": "scene",
                        "text": "",
                        "sceneId": "5",
                        "targetYaw":-65,
                        "targetPitch": 2
                    },
                    {
                        "pitch": -0.6,
                        "yaw": 180,
                        "type": "scene",
                        "text": " ",
                        "sceneId": "6.1",
                        "targetYaw":180,
                        "targetPitch": 2  
                    }] 
                    },
                "7":{  
                    "title": "Edificio F",
                    "hfov": 100,
                    "yaw": 5,
                    "type": "equirectangular",
                    "panorama": "img/7.jpg",
                    "hotSpots": [
                    {//aun no foto
                        "pitch": -1,
                        "yaw": 2.3,
                        "type": "scene",
                        "text": "",
                        "sceneId": "8"
                    },
                    {
                        "pitch": -0.6,
                        "yaw": 180,
                        "type": "scene",
                        "text": " ",
                        "sceneId": "6.1",
                        "targetYaw":-75,
                        "targetPitch": 2  
                    }] 
                    },
                "8": {
                    "title": "Campus 1",
                    "hfov": 110,//zoom
                    "yaw": 0,//posicion en la que se ve en grados
                    "type": "equirectangular",
                    "panorama": "img/8.jpg",
                    "hotSpots": [
                    {///F
                        "pitch": -0.6,
                        "yaw":0 ,
                        "type": "scene",
                        "text": "",
                        "sceneId": "8.1",
                        "targetYaw":0,
                        "targetPitch": 2
                    },
                    {
                        "pitch": -3.3,
                        "yaw": 187,
                        "type": "scene",
                        "text": "",
                        "sceneId": "7",
                        "targetYaw": 180,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },
                "14": {
                    "title": "Campus 1",
                    "hfov": 110,//zoom
                    "yaw": 0,//posicion en la que se ve en grados
                    "type": "equirectangular",
                    "panorama": "img/14.jpg",
                    "hotSpots": [
                    {///F
                        "pitch": -0.6,
                        "yaw":0 ,
                        "type": "scene",
                        "text": "",
                        "sceneId": "15",
                        "targetYaw":180,
                        "targetPitch": 2
                    },
                    {
                        "pitch": -3.3,
                        "yaw": 178,
                        "type": "scene",
                        "text": "",
                        "sceneId": "1.2",
                        "targetYaw": 0,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },
                 "15": {
                    "title": "Campus 1",
                    "hfov": 110,//zoom
                    "yaw": 0,//posicion en la que se ve en grados
                    "type": "equirectangular",
                    "panorama": "img/15.jpg",
                    "hotSpots": [
                    {///F
                        "pitch": -0.6,
                        "yaw":95 ,
                        "type": "scene",
                        "text": "",
                        "sceneId": "6",
                        "targetYaw":35,
                        "targetPitch": 2
                    },
                    {
                        "pitch": 0.3,
                        "yaw": 6,
                        "type": "scene",
                        "text": "",
                        "sceneId": "14",
                        "targetYaw": 170,//posicion de regreso en garados
                        "targetPitch": 2  
                    }]
                    },

                }
            });