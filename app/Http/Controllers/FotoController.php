<?php

namespace App\Http\Controllers;
use App\Foto;

use Illuminate\Http\Request;
use App\Http\Requests;

class FotoController extends Controller
{
    public function index (){
    	// $fotos = Foto::all();
    	$fotos = \DB::table('fotos_360')->select('foto')->get();
    	// return view('prueba.index', compact("fotos"));
    	return $fotos;
    }
}
